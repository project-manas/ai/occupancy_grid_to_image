cmake_minimum_required(VERSION 3.5)
project(occupancy_grid_to_image)

find_package(catkin REQUIRED COMPONENTS sensor_msgs nav_msgs tf message_filters)

include_directories(include ${catkin_INCLUDE_DIRS})

catkin_package(
   INCLUDE_DIRS include
   LIBRARIES ${PROJECT_NAME}
   CATKIN_DEPENDS roscpp)

add_executable(occupancy_grid_to_image src/occupancy_grid_to_image.cpp)
target_link_libraries(occupancy_grid_to_image ${catkin_LIBRARIES})