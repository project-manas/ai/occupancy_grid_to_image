# Occupancy Grid to Image
Converts nav_msgs::OccupancyGrid to sensor_msgs::Image.  

## Publications
- **/map_image (sensor_msgs/Image)**  
      Converted image.

## Subscriptions
- **/map (nav_msgs/OccupancyGrid)**  
      Map to be converted.
- **/odom (nav_msgs/Odometry)**  
      Odometry of robot.

*Note: The functionality of giving the pose of robot on the map is yet to be added.*
