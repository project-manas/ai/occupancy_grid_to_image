#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <nav_msgs/OccupancyGrid.h>
#include <nav_msgs/Odometry.h>
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>

typedef message_filters::sync_policies::ApproximateTime<nav_msgs::OccupancyGrid, nav_msgs::Odometry> sync_policy;

class OccupancyGridToImage {
public:
  ros::Publisher image_pub;

  void callback(const nav_msgs::OccupancyGridConstPtr& map, const nav_msgs::OdometryConstPtr& odom) {
    sensor_msgs::Image img;
    img.height = map->info.height;
    img.width = map->info.width;
    img.data.resize(map->data.size());
    img.encoding = "mono8";
    int map_data;
    uint image_data;
    for(int i = 0; i < img.height; ++i) {
    	for(int j = 0; j < img.width; ++j) {
    		std::memcpy(&map_data, &map->data[i * map->info.width + map->info.width - j], 1);
    		image_data = map_data == 255 ? 127 : 255 - (uint)map_data * 2.55;
    		std::memcpy(&img.data[i * img.width + j], &image_data, 1);
    	}
    }
    image_pub.publish(img);
  }
};

int main(int argc, char** argv) {
  ros::init(argc, argv, "occupancy_grid_to_image_node");
  ros::NodeHandle nh("~");

  OccupancyGridToImage otoi;
  otoi.image_pub = nh.advertise<sensor_msgs::Image>("map_image", 10);
  message_filters::Subscriber<nav_msgs::OccupancyGrid> map_sub(nh, "/map", 10);
  message_filters::Subscriber<nav_msgs::Odometry> odom_sub(nh, "/odom", 10);
  message_filters::Synchronizer<sync_policy> sync(sync_policy(10), map_sub, odom_sub);
  sync.registerCallback(boost::bind(&OccupancyGridToImage::callback, otoi, _1, _2));

  ros::spin();
  return 0;
}